import { NegociacaoController } from "./controllers/negociacao-controller.js";
const controller = new NegociacaoController();
const form = document.querySelector('.form');
form.addEventListener('submit', event => {
    event.preventDefault(); //pagina nao vai dar mais o refresh quando for dado o submit do formulario
    controller.adiciona();
});
